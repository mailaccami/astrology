<?php
/**
 * @var \common\models\User $user
 * @var \common\models\Course $course
 * @var \common\models\Lesson $lesson
 * @var \common\models\Module $module
 */

use yii\helpers\Url;
use common\models\HomeworkForm;
use yii\widgets\ActiveForm;
use trntv\filekit\widget\Upload;

$this->title = $lesson->title;

?>


<div class="content">
    <div id="scene">
        <svg class="svg-sprite-icon icon-zodiac-1" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-1"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-2" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-2"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-3" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-3"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-4" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-4"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-5" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-5"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-6" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-6"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-7" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-7"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-8" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-8"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-9" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-9"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-10" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-10"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-11" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-11"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-12" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-12"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-13" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-13"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-14" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-14"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-15" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-15"></use>
        </svg><img class="constellation-1" src="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-1.png" data-depth="0.3"><img class="constellation-2" src="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-2.png" data-depth="0.4"><img class="constellation-3" src="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-3.png" data-depth="0.5"><img class="constellation-4" src="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-4.png" data-depth="0.6"><img class="constellation-5" src="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-5.png" data-depth="0.7">
    </div>
    <section class="lessons">
        <div class="container">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                </li>
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="<?=Url::to(["/user/default/courses"])?>">Мои курсы</a>
                </li>
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="<?=\yii\helpers\Url::to(['/user/default/course', 'slug' => $course->slug])?>"><?=$course->title?></a>
                </li>
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="<?=\yii\helpers\Url::to(['/user/default/module', 'id' => $module->id])?>"><?=$module->title?></a>
                </li>
                <li class="breadcrumbs__item"><span class="breadcrumbs__text"><?=$lesson->title?></span>
                </li>
            </ul>
            <div class="lessons__grid">
                <?php if(empty(!$lesson->video_link)):?>
                    <div class="lessons__video card" data-aos="fade-down-right">
                        <div class="card__ribbon">
                            <div class="card__counter"><span>урок</span><br> <?=$lesson->num?></div>
                        </div><a class="card__video" data-fancybox href="<?=$lesson->video_link?>">
                            <div class="card__play"></div><img class="card__img lessons__img" src="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/content/placeholder.jpg"></a>
                    </div>
                <?php endif;?>
                <div class="lessons__content" data-aos="fade-down-left">
                    <div class="title lessons__title"><?=$lesson->title?></div>
                    <?=$lesson->description?>


                    <?php if($lesson->fileUrl):?>
                        <a class="btn btn_big  btn_default " href="<?=str_replace("http","https",$lesson->fileUrl)?>" target="_blank"><span class="btn__label">Задание по уроку</span></a>
                    <?php endif;?>


                </div>
                <div class="lessons__professor" data-aos="fade-up-right"><img class="lessons__photo" src="<?=$course->teacher->userProfile->avatar?>">
                    <div class="lessons__anthroponym">
                        <p>Преподаватель</p>
                        <p class="text text_fancy lessons__name"><?=$course->teacher->name?></p><a class="btn btn_big  btn_transparent lessons__btn" href="<?=$course->teacher->telegram?>">
                            <svg class="svg-sprite-icon icon-send btn__icon_left btn__icon btn__icon_theme_transparent">
                                <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#send"></use>
                            </svg><span class="btn__label">Написать преподавателю</span></a>
                    </div>
                </div>
                    <?php $homework_form = new HomeworkForm(); ?>
                    <?php $form = ActiveForm::begin([
                        'id' => 'homework-form',
                        'options' => [
                            'class' => 'form form_size_sm form_theme_solution'
                         ],
                        'action' => Url::to('/form/homework-save'),
                        'enableAjaxValidation' => true,
                        'validationUrl' =>  Url::to('/form/homework-validate'),
                    ]); ?>
                    <div class="form__body">

                        <dl class="form-cell form-cell_full_width">
                            <!--<dt class="form-cell__hline">
                                <label for="Решение_задания">Решение задания</label>
                            </dt>-->
                            <dd class="form-cell__field-wrapper">
                                <?= $form->field($homework_form, 'message')->textarea(['class' => 'answer__input form-cell__field', 'placeholder'=>'Решение задания'])->label(false) ?>
                                <?= $form->field($homework_form, 'user_id')->hiddenInput(['value'=>$user->id])->label(false) ?>
                                <?= $form->field($homework_form, 'teacher_id')->hiddenInput(['value'=>$course->teacher->id])->label(false) ?>
                                <?= $form->field($homework_form, 'lesson_name')->hiddenInput(['value'=>$course->title.": ".$lesson->title])->label(false) ?>
                            </dd>
                        </dl>
                        <button class="btn  btn_default form__btn" type="submit" style="margin-top: 40px; margin-left: 1rem;"><span class="btn__label">Отправить</span>
                        </button>
                        <dl class="form-cell form-cell_type_file">

                            <dd class="form-cell__field-wrapper form-cell__field-wrapper_type_file">
                                <div style="display: flex;">
                                <label for="userfile_field">
                                    <svg width="18" height="20" viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M17.2375 7.41976C16.9438 7.11141 16.4689 7.11279 16.1768 7.42277L7.16085 16.9911C5.99114 18.2258 4.09135 18.2258 2.91946 16.9894C1.74845 15.7527 1.74845 13.7474 2.9196 12.5112L12.2024 2.66115C12.9327 1.89034 14.12 1.89034 14.8524 2.66282C15.5846 3.43568 15.5846 4.68878 14.8522 5.46186L7.16225 13.579C7.16176 13.5795 7.16134 13.5801 7.16085 13.5806C6.86788 13.8882 6.39442 13.8877 6.10206 13.5791C5.8092 13.27 5.8092 12.769 6.10206 12.4599L9.81394 8.54094C10.1068 8.23173 10.1068 7.73045 9.81383 7.42132C9.52091 7.11219 9.046 7.11223 8.75315 7.42143L5.0413 11.3403C4.16269 12.2677 4.16269 13.7713 5.04137 14.6987C5.92001 15.6262 7.34444 15.6262 8.22312 14.6987C8.22414 14.6976 8.22495 14.6965 8.22593 14.6954L15.9129 6.5814C17.2311 5.18999 17.2311 2.93436 15.9129 1.54295C14.5945 0.152351 12.4578 0.152351 11.1403 1.54295L1.85751 11.393C0.101974 13.2461 0.101974 16.2537 1.85877 18.109C3.61673 19.9637 6.46607 19.9637 8.22298 18.1092L17.2404 8.53938C17.5325 8.22936 17.5312 7.72811 17.2375 7.41976Z" fill="black"/>
                                    </svg>
                                    Решение
                                </label>
                                <?php echo $form->field($homework_form, 'file')->widget(
                                    Upload::class,
                                    [
                                        'id'=>'userfile_field',
                                        'url' => ['file-upload'],
                                        'previewImage' => false
                                    ]
                                )->label(false)?>
                                </div>
                            </dd>
                        </dl>
                    </div>
                    <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>
</div>
