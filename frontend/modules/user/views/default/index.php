<?php

use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\base\MultiModel */
/* @var $form yii\bootstrap4\ActiveForm */

$this->title = Yii::t('frontend', 'User Settings')
?>



<div class="content">
    <div id="scene">
        <svg class="svg-sprite-icon icon-zodiac-1" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-1"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-2" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-2"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-3" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-3"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-4" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-4"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-5" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-5"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-6" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-6"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-7" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-7"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-8" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-8"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-9" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-9"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-10" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-10"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-11" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-11"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-12" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-12"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-13" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-13"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-14" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-14"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-15" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-15"></use>
        </svg><img class="constellation-1" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-1.png" data-depth="0.3"><img class="constellation-2" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-2.png" data-depth="0.4"><img class="constellation-3" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-3.png" data-depth="0.5"><img class="constellation-4" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-4.png" data-depth="0.6"><img class="constellation-5" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-5.png" data-depth="0.7">
    </div>
    <section class="account">
        <div class="container">
            <div class="title">Личные данные</div>
            <?php $form = ActiveForm::begin(); ?>
            <div class="account__grid">
                <div class="account__photo" data-aos="zoom-out">
                    <?php echo $form->field($model->getModel('profile'), 'picture')->widget(
                        Upload::class,
                        [
                            'url' => ['avatar-upload']
                        ]
                    )->label(false)?>
                    <dl class="form-cell form-cell_type_file account__file-upl">
                        <dd class="form-cell__field-wrapper form-cell__field-wrapper_type_file">
                            <div class="form-cell__file-field btn btn_transparent btn_big"><label for="w1" class="form-cell__file-text">Загрузить файл</label><span class="form-cell__file-btn"></span></div>
                        </dd>
                    </dl><span style="font-size: 14px;">до 5 Мб</span><span class="form__value "></span>
                </div>
                <form class="form form_size_sm" id="form-profile" action="#" method="post" data-aos="zoom-out">
                    <div class="form form_size_sm">
                        <div class="form__header">
                            <div class="form__subtitle">Заполните данные своего профиля, чтобы полноценно пользоваться всеми функциями сервиса</div>
                        </div>
                        <div class="form__body">
                            <dl class="form-cell form-cell_third_width">
                                <dt class="form-cell__hline">
                                    <label for="Имя">Имя</label>
                                </dt>
                                <dd class="form-cell__field-wrapper">
                                    <?php echo $form->field($model->getModel('account'), 'name')->textInput(['class' => 'form-cell__field', 'id'=>'personal-name'])->label(false) ?>
                                </dd>
                            </dl>
                            <dl class="form-cell form-cell_third_width">
                                <dt class="form-cell__hline">
                                    <label for="Email">Email</label>
                                </dt>
                                <dd class="form-cell__field-wrapper">
                                    <?php echo $form->field($model->getModel('account'), 'email')->textInput(['class' => 'form-cell__field', 'id'=>'personal-email'])->label(false) ?>
                                </dd>
                            </dl>
                            <dl class="form-cell form-cell_third_width">
                                <dt class="form-cell__hline">
                                    <label for="Фамилия">Фамилия</label>
                                </dt>
                                <dd class="form-cell__field-wrapper">
                                    <?php echo $form->field($model->getModel('profile'), 'lastname')->textInput(['class' => 'form-cell__field', 'id'=>'personal-surname'])->label(false) ?>
                                </dd>
                            </dl>
                            <dl class="form-cell form-cell_third_width">
                                <dt class="form-cell__hline">
                                    <label for="Телефон">Телефон</label>
                                </dt>
                                <dd class="form-cell__field-wrapper">
                                    <?php echo $form->field($model->getModel('account'), 'phone')->textInput(['class' => 'form-cell__field', 'id'=>'personal-phone'])->label(false) ?>
                                </dd>
                            </dl>
                            <dl class="form-cell form-cell_third_width">
                                <dt class="form-cell__hline">
                                    <label for="Telegram">Telegram</label>
                                </dt>
                                <dd class="form-cell__field-wrapper">
                                    <?php echo $form->field($model->getModel('account'), 'telegram')->textInput(['class' => 'form-cell__field', 'id'=>'personal-telegram'])->label(false) ?>
                                </dd>
                            </dl>
                        </div>
                        <div class="form__btn-wrapp">
                            <button class="btn btn_big btn_default form__btn form__btn btn-green" ><span class="btn__label">Сохранить изменения</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </section>
</div>


