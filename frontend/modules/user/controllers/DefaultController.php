<?php

namespace frontend\modules\user\controllers;

use common\base\MultiModel;
use common\models\Lesson;
use common\models\Module;
use frontend\modules\user\models\AccountForm;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class DefaultController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'avatar-upload' => [
                'class' => UploadAction::class,
                'deleteRoute' => 'avatar-delete',
            ],
            'avatar-delete' => [
                'class' => DeleteAction::class
            ],
            'file-upload' => [
                'class' => UploadAction::class,
                'deleteRoute' => 'file-delete',
            ],
            'file-delete' => [
                'class' => DeleteAction::class
            ]
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $accountForm = new AccountForm();
        $accountForm->setUser(Yii::$app->user->identity);

        $model = new MultiModel([
            'models' => [
                'account' => $accountForm,
                'profile' => Yii::$app->user->identity->userProfile
            ]
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $locale = $model->getModel('profile')->locale;
            Yii::$app->session->setFlash('forceUpdateLocale');
            Yii::$app->session->setFlash('alert', [
                'options' => ['class' => 'alert-success'],
                'body' => Yii::t('frontend', 'Your account has been successfully saved', [], $locale)
            ]);
            return $this->refresh();
        }
        return $this->render('index', ['model' => $model]);
    }
    /**
     * @return string|\yii\web\Response
     */
    public function actionCourses()
    {

        $user = Yii::$app->user->identity;

        return $this->render('courses', ['model' => $user]);
    }
    public function actionCourse($slug)
    {

        $user = Yii::$app->user->identity;

        $course = $user->getCourses()->where(['slug'=>$slug])->one();

        return $this->render('viewCourse', [
            'user' => $user,
            'course' => $course
        ]);
    }
    public function actionModule($id)
    {

        $user = Yii::$app->user->identity;

        $module = Module::find()->where(['id'=>$id])->one();

        return $this->render('viewModule', [
            'user' => $user,
            'module' => $module
        ]);
    }
    public function actionLesson($id, $course, $module)
    {

        $user = Yii::$app->user->identity;

        $course = $user->getCourses()->where(['slug'=>$course])->one();

        $module = $course->getModules()->where(['id'=>$module])->one();

        $lesson = $module->getLessons()->where(['id'=>$id])->one();

        $prevLesson = Lesson::find()->where(["<", "weight", $lesson->weight])->andWhere(['module'=>$module->id])->orderBy(["weight"=>SORT_ASC])->one();
        $nextLesson = Lesson::find()->where([">", "weight", $lesson->weight])->andWhere(['module'=>$module->id])->orderBy(["weight"=>SORT_ASC])->one();

        return $this->render('viewLesson', [
            'user' => $user,
            'course' => $course,
            'module' => $module,
            'lesson' => $lesson,
            'prevLesson' => $prevLesson,
            'nextLesson' => $nextLesson,
        ]);
    }
}
