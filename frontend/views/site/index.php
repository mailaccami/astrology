<?php

use yii\helpers\Url;

/**
 * @var yii\web\View $this
 */

$this->title = Yii::$app->name;
?>

<div class="content">
        <div id="scene">
            <svg class="svg-sprite-icon icon-zodiac-1" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-1"></use>
            </svg>
            <svg class="svg-sprite-icon icon-zodiac-2" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-2"></use>
            </svg>
            <svg class="svg-sprite-icon icon-zodiac-3" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-3"></use>
            </svg>
            <svg class="svg-sprite-icon icon-zodiac-4" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-4"></use>
            </svg>
            <svg class="svg-sprite-icon icon-zodiac-5" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-5"></use>
            </svg>
            <svg class="svg-sprite-icon icon-zodiac-6" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-6"></use>
            </svg>
            <svg class="svg-sprite-icon icon-zodiac-7" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-7"></use>
            </svg>
            <svg class="svg-sprite-icon icon-zodiac-8" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-8"></use>
            </svg>
            <svg class="svg-sprite-icon icon-zodiac-9" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-9"></use>
            </svg>
            <svg class="svg-sprite-icon icon-zodiac-10" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-10"></use>
            </svg>
            <svg class="svg-sprite-icon icon-zodiac-11" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-11"></use>
            </svg>
            <svg class="svg-sprite-icon icon-zodiac-12" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-12"></use>
            </svg>
            <svg class="svg-sprite-icon icon-zodiac-13" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-13"></use>
            </svg>
            <svg class="svg-sprite-icon icon-zodiac-14" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-14"></use>
            </svg>
            <svg class="svg-sprite-icon icon-zodiac-15" data-depth="0.1">
                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-15"></use>
            </svg><img class="constellation-1" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-1.png" data-depth="0.2"><img class="constellation-2" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-2.png" data-depth="0.3"><img class="constellation-3" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-3.png" data-depth="0.4"><img class="constellation-4" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-4.png" data-depth="0.5"><img class="constellation-5" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-5.png" data-depth="0.6">
        </div>
        <section class="prologue"><img class="prologue__planet" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/planet.png">
            <div class="container">
                <div class="text text_fancy">Karina Demchenko</div>
                <div class="title prologue__title">Ведическая астрология и психоанализ</div>
                <p class="prologue__text-1">- синтез современной науки и древних знаний в уникальном авторском курсе.</p>
                <p class="prologue__text-2">Обучающие  программы  для  тех,   кто  устал  брести   по  жизни  с  завязанными глазами.</p><a class="btn   btn_default prologue__btn" href="<?=Url::to(["/courses"])?>"><span class="btn__label">Узнать подробнее</span></a>
            </div>
        </section>
        <section class="greetings">
            <div class="container">
                <svg class="svg-sprite-icon icon-quotes">
                    <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#quotes"></use>
                </svg>
                <div class="title">Намасте!</div>
                <div class="greetings__grid">
                    <div class="greetings__content" data-aos="slide-right">
<p>Меня зовут Карина Демченко, я ведический астролог и практикующий психолог.</p>
<p>Работаю в двух направлениях &ndash; ведическая астрология и психоанализ.</p>
<p>&nbsp;</p>
<p>Две науки &ndash; астрология и психология тесно связаны между собой, хотя на первый взгляд очень разные. На самом деле цель у них одна &ndash; помочь человеку в познании себя, мира, оказать поддержку на пути следования&nbsp;своей Дхармы раскрывая свой личностный потенциал на протяжении всей жизни.</p>
<p>&nbsp;</p>
<p>Астрология изучает причинно-следственные связи,&nbsp;прописанные Вселенной как закон, расширяет горизонты сознания и показывает перспективы развития человека, дает ключ к пониманию нашего бытия.</p>
<p>Психоанализ, как одно из направлений психологии,&nbsp;изучает причинно-следственные связи в рамках психики человека, раскрывает причины психологических проблем, разочарований и неудач.&nbsp;Современные методы психоанализа позволяют в кратчайшие сроки разрешать проблемы в личной, семейной, профессиональной и других сферах жизни человека.</p>
<p>&nbsp;</p>
<p>Синтез двух направлений позволяет разрешать сложные жизненные ситуации в кратчайшие сроки, раскрывая личностный потенциал человека, не уводя его с пути кармических задач и желаний души.</p>
<p>&nbsp;</p>
<p>Для тех, кто хочет приобрести новую профессию&nbsp;астро-психолога или расширить уже имеющиеся знания в астрологии и психологии, добро пожаловать на обучающие курсы.</p>
<p>Анализируя свой гороскоп в процессе обучения и используя техники современного психоанализа Вы в скором времени освободитесь от прошлых обид, боли и разочарований. Почувствуете внутреннюю легкость и уверенность в себе, необходимые каждому успешному человеку.&nbsp;</p>
<p>&nbsp;</p>
<p>Для тех, кто не готов обучаться, но хочет изменить свою жизнь, добавить ярких красок в ежедневную рутину жизни, раскрыть свой личностный потенциал - добро пожаловать на астро-психологическую консультацию.&nbsp;(см. подробнее на странице &laquo;Консультации&raquo;)</p>
<p>&nbsp;</p>
<p>Основная задача каждого человека, максимально раскрыть свой личностный потенциал, реализовать желания души, поэтому свое предназначение вижу в оказании быстрой психологической и астрологической помощи.</p>
<p>Страницы этого сайта я наполнила полезной и важной информацией, которая может быть необходима для Вас именно сейчас.</p>
<p>&nbsp;</p>
<p><em>&laquo;Жизнь дана человеку, прежде всего для самосознания. Это значит, что человек должен понять, кто он, что представляет собой этот мир и что является высшей истиной.&raquo; [Ш.Б. п. 1, гл.1, текст 10]</em></p>
<p>&nbsp;</p>
<p class="text text_bold">Желаю Вам приятного и благодатного САМОПОЗНАНИЯ!</p>
                <div class="text text_fancy text_align_right greetings__sign">Искренне ваша, Карина Демченко!</div>
                    </div>
                    <div class="greetings__photo" data-aos="slide-left"><img src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/content/owner.png"></div>
                </div>
            </div>
        </section>
        <section class="main-courses">
            <div class="container">
                <div class="title">Наши курсы</div>
                <div class="cards-grid">

                    <?php $i = 0; foreach (\common\models\Course::find()->orderBy(['weight' => SORT_ASC])->limit(6)->all() as $course): $i++; ?>
                        <div class="card" data-aos="fade-up" data-aos-delay="50">
                            <div class="card__header">
                                <div class="card__ribbon">
                                    <div class="card__counter"><?php if($i < 10) { echo '0';} echo $i; ?></div>
                                </div><a class="card__video">
                                   <img class="card__img" src="<?=$course->image?>"/></a>
                            </div>
                            <div class="card__body">
                                <div class="card__title subtitle"><?=$course->title?></div>
                                <div class="card__content"><?=$course->short_content?></div><a class="btn   btn_transparent card__btn" href="<?=\yii\helpers\Url::to(['/courses/view', 'slug'=>$course->slug])?>"><span class="btn__label">Перейти к курсу</span></a>
                            </div>
                        </div>
                    <?php endforeach;?>


                </div>
                <div class="btn-wrapp main-courses__btn-wrapp"><a class="btn   btn_default" href="<?=Url::to(["/courses"])?>"><span class="btn__label">Открыть все крусы</span></a>
                </div>
            </div>
        </section>
        <section class="advantages">
            <div class="container">
                <div class="text text_fancy">Что делает мое обучение особенным?</div>
                <div class="title advantages__title">Какие преимущества получаете именно вы?</div>
                <div class="cards-grid">
                    <div class="advantages__card adv-card" data-aos="zoom-out" data-aos-delay="50">
                        <div class="adv-card__icon">
                            <svg class="svg-sprite-icon icon-no-extra">
                                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#no-extra"></use>
                            </svg>
                        </div>
                        <div class="adv-card__title subtitle">Ничего лишнего</div>
                        <div class="adv-card__descr">Из множества курсов выбирайте только то, что действительно нужно и легко составляйте свой личный учебный план.</div>
                    </div>
                    <div class="advantages__card adv-card" data-aos="zoom-out" data-aos-delay="550">
                        <div class="adv-card__icon">
                            <svg class="svg-sprite-icon icon-comfort">
                                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#comfort"></use>
                            </svg>
                        </div>
                        <div class="adv-card__title subtitle">Удобство</div>
                        <div class="adv-card__descr">занимайтесь в любое время, в любом месте, в своем личном онлайн-кабинете.</div>
                    </div>
                    <div class="advantages__card adv-card" data-aos="zoom-out" data-aos-delay="1050">
                        <div class="adv-card__icon">
                            <svg class="svg-sprite-icon icon-multi">
                                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#multi"></use>
                            </svg>
                        </div>
                        <div class="adv-card__title subtitle">Мульти-направленность</div>
                        <div class="adv-card__descr">изучайте одновременно три направления - астрологию, нейропсихологию и психоанализ. Синтезируйте научные знания, достигайте высокого уровня возможностей, копите ресурс для успешного будущего.</div>
                    </div>
                </div>
            </div>
        </section>
        <section class="reviews" id="reviews">
            <div class="container">
                <div class="title">Отзывы</div>
                <div class="swiper-container reviews__slider reviews__slider_desktop">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="reviews__slides-grid">
								<?php foreach($reviews as $review):?>
                                <div class="reviews__card" data-aos="zoom-in">
                                    <div class="reviews__date"><?=$review->date?></div>
                                    <div class="reviews__author subtitle"><?=$review->name?></div>
                                    <div class="reviews__content"><?=$review->comment?></div>
                                </div>
								<?php endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-container reviews__slider reviews__slider_mobile">
                    <div class="swiper-wrapper">
							<?php foreach($reviews as $review):?>
								<div class="swiper-slide">
									<div class="reviews__card" data-aos="zoom-in">
										<div class="reviews__date"><?=$review->date?></div>
										<div class="reviews__author subtitle"><?=$review->name?></div>
										<div class="reviews__content"><?=$review->comment?></div>
									</div>
								</div>
							<?php endforeach;?>
                    </div>
                </div>
                <div class="btn-wrapp reviews__btn-wrapp"><a class="btn   btn_default modal-btn" href="#modal-review"><span class="btn__label">Оставить отзыв</span></a>
                </div>
            </div>
        </section>
    </div>

