<?php
/**
 * @var yii\web\View $this
 * @var common\models\Page $model
 */

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;

$this->title = $model->title;
?>
<div class="content">
    <div id="scene">
        <svg class="svg-sprite-icon icon-zodiac-1" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-1"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-2" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-2"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-3" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-3"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-4" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-4"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-5" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-5"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-6" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-6"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-7" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-7"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-8" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-8"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-9" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-9"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-10" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-10"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-11" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-11"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-12" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-12"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-13" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-13"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-14" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-14"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-15" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-15"></use>
        </svg><img class="constellation-1" src="./<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-1.png" data-depth="0.3"><img class="constellation-2" src="./<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-2.png" data-depth="0.4"><img class="constellation-3" src="./<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-3.png" data-depth="0.5"><img class="constellation-4" src="./<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-4.png" data-depth="0.6"><img class="constellation-5" src="./<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-5.png" data-depth="0.7">
    </div>
    <section class="content-section">
        <div class="container">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                </li>
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="<?=Url::to(["/consultations"])?>">Консультации</a>
                </li>
                <li class="breadcrumbs__item"><span class="breadcrumbs__text"><?php echo Html::encode($model->title) ?></span>
                </li>
            </ul>
            <div class="title"><?php echo Html::encode($model->title) ?></div>
            <div class="content-section__subject">
                <?php echo $model->body ?>
            </div>
        </div>
    </section>
    <section class="form-section">
        <div class="container">
            <form class="form" id="form-request" action="#" method="post">
                <div class="form__header">
                    <div class="form__title">Заполните заявку</div>
                    <div class="form__subtitle">и наши специалисты свяжуться с вами</div>
                </div>
                <div class="form__body">
                    <dl class="form-cell form-cell_half_width">
                        <dt class="form-cell__hline">
                            <label for="Имя">Имя</label>
                        </dt>
                        <dd class="form-cell__field-wrapper">
                            <input class="form-cell__field  " type="text" id="Имя" name="name" placeholder="Имя"/>
                        </dd>
                    </dl>
                    <dl class="form-cell form-cell_half_width">
                        <dt class="form-cell__hline">
                            <label for="Телефон">Телефон</label>
                        </dt>
                        <dd class="form-cell__field-wrapper">
                            <input class="form-cell__field  " type="text" id="Телефон" name="phone" placeholder="+7 (___) ___-__-__"/>
                        </dd>
                    </dl>
                    <dl class="form-cell form-cell_full_width">
                        <dt class="form-cell__hline">
                            <label for="Сообщение">Сообщение</label>
                        </dt>
                        <dd class="form-cell__field-wrapper">
                            <textarea class="form-cell__field  " type="textarea" id="Сообщение" name="message" placeholder="Сообщение"></textarea>
                        </dd>
                    </dl>
                    <div class="form__policy">Нажимая на кнопку, вы даете согласие на обработку своих персональных данных <br><a href="#">Политика конфиденциальности</a></div>
                </div>
                <div class="form__btn-wrapp">
                    <button class="btn  btn_default form__btn" type="submit"><span class="btn__label">Отправить</span>
                    </button>
                </div>
            </form>
        </div>
    </section>
</div>

