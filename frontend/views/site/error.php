<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var string $name
 * @var string $message
 * @var Exception $exception
 */

$this->title = $name;
?>

<section class="mainblock">
    <img class="square1" src="<?= Yii::getAlias('@web') ?>/theme/crowdwiz/dist/img/useful/square1.svg" alt="">
    <div class="container">
        <div class="inner">
            <div class="left">
                <h1><?php echo Yii::t('frontend', 'Oops! There was an error...') ?></h1>
                <h3><?php echo Html::encode($this->title) ?></h3>

                <div class="alert alert-warning">
                    <span class="fas fa-exclamation-triangle"></span> <?php echo nl2br(Html::encode($message)) ?>
                </div>
            </div>
            <div class="right">
                <img class="img" src="<?= Yii::getAlias('@web') ?>/theme/crowdwiz/dist/img/mainblock/img.png" alt="">
            </div>
        </div>
    </div>
</section>

