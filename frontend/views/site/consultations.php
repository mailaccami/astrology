<?php

use yii\widgets\LinkPager;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var frontend\models\search\ArticleSearch $searchModel
 * @var array $categories
 * @var array $archive
 */

$this->title = Yii::t('frontend', 'Консультации')
?>


<div class="content">
    <div id="scene">
        <svg class="svg-sprite-icon icon-zodiac-1" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-1"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-2" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-2"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-3" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-3"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-4" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-4"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-5" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-5"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-6" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-6"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-7" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-7"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-8" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-8"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-9" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-9"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-10" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-10"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-11" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-11"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-12" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-12"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-13" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-13"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-14" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-14"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-15" data-depth="0.1">
            <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-15"></use>
        </svg><img class="constellation-1" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-1.png" data-depth="0.3"><img class="constellation-2" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-2.png" data-depth="0.4"><img class="constellation-3" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-3.png" data-depth="0.5"><img class="constellation-4" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-4.png" data-depth="0.6"><img class="constellation-5" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-5.png" data-depth="0.7">
    </div>
    <section class="consultations">
        <div class="container">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                </li>
                <li class="breadcrumbs__item"><span class="breadcrumbs__text">Консультации</span>
                </li>
            </ul>
            <div class="title">Консультации</div>
            <div class="cards-grid">
                <?php foreach ($dataProvider->models as $model):?>
                    <div class="card" data-aos="flip-up" data-aos-delay="50">
                        <div class="card__body card__body_size_sm">
                            <div class="card__title subtitle"><?=$model->title?></div>
                            <div class="card__content card__content_size_base"><?=$model->short_content?></div><a class="btn   btn_transparent card__btn" href="<?=\yii\helpers\Url::to(['/site/consultations-view', 'slug'=>$model->slug])?>"><span class="btn__label">Подробнее</span></a>
                        </div>
                    </div>
                <?php endforeach;?>

            </div>

            <?= LinkPager::widget([
                'pagination' => $dataProvider->getPagination(),
                'activePageCssClass' => 'is-active',
                'pageCssClass' => 'pagination__item',
            ]); ?>

        </div>
    </section>
</div>