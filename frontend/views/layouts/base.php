<?php
/**
 * @var yii\web\View $this
 * @var string $content
 */

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\models\OrderForm;
use common\models\Review;
use common\models\QuestionForm;

$this->beginContent('@frontend/views/layouts/_clear.php')
?>

    <div class="wrapper">
    <div id="site-overlay">
        <div class="nav-mobile"></div>
    </div>
    <header class="header">
        <div class="header__wrapp">
            <div class="logo"><a class="logo__link" href="/"><img class="logo__img" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/logo.png"></a></div>
            <div class="header__content">
                <div class="header__top-panel">
                    <ul class="contacts header__contacts">
                        <li class="contacts__item contacts__item_mobile_sandwich"><a class="contacts__link" href="tel:+79896174572">
                                <div class="contacts__icon">
                                    <svg class="svg-sprite-icon icon-phone">
                                        <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#phone"></use>
                                    </svg>
                                </div>
                                <div class="contacts__text header__text">+79896174572</div></a>
                        </li>
                        <li class="contacts__item">
                            <div class="contacts__icon">
                                <svg class="svg-sprite-icon icon-calendar">
                                    <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#calendar"></use>
                                </svg>
                            </div>
                            <div class="contacts__text header__text">с 9.00 до 19.00 по Мск</div>
                        </li>
                        <li class="contacts__item contacts__item_mobile_sandwich"><a class="contacts__link" href="tel:+380953942294">
                                <div class="contacts__icon">
                                    <svg class="svg-sprite-icon icon-phone">
                                        <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#phone"></use>
                                    </svg>
                                </div>
                                <div class="contacts__text header__text">+380953942294</div></a>
                        </li>
                        <li class="contacts__item"><a class="contacts__link" href="mailto:astropsihology@mail.ua">
                                <div class="contacts__icon">
                                    <svg class="svg-sprite-icon icon-envelop">
                                        <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#envelop"></use>
                                    </svg>
                                </div>
                                <div class="contacts__text header__text">astropsihology@mail.ua</div></a>
                        </li>
                    </ul>
                    <ul class="socials header__socials">
                        <li class="socials__item"><a class="socials__link" href="whatsapp://send?phone=380953942294">
                                <svg class="svg-sprite-icon icon-whatsapp socials__icon">
                                    <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#whatsapp"></use>
                                </svg></a></li>
                        <li class="socials__item"><a class="socials__link" href="viber://add?number=380953942294">
                                <svg class="svg-sprite-icon icon-viber socials__icon">
                                    <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#viber"></use>
                                </svg></a></li>
                        <li class="socials__item"><a class="socials__link" href="https://t.me/karina_dem4enko">
                                <svg class="svg-sprite-icon icon-telegram socials__icon">
                                    <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#telegram"></use>
                                </svg></a></li>
                    </ul>
                    <?php if(Yii::$app->user->isGuest): ?>
                        <div class="header__login login"><a class="login__link modal-btn" href="#modal-login">
                                <svg class="svg-sprite-icon icon-login">
                                    <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#login"></use>
                                </svg>
                                <div class="login__text">Войти</div></a></div>
                    <?php else: ?>
                        <div class="header__dropdown">
                            <div class="header__username"><?=Yii::$app->user->identity->name?></div>
                            <svg class="svg-sprite-icon icon-caret-down">
                                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#caret-down"></use>
                            </svg>
                            <ul class="header__drop-content">
                                <li class="header__drop-item"><a class="header__drop-link" href="<?=Url::to(['/user/default/index'])?>">Профиль</a></li>
                                <li class="header__drop-item"><a class="header__drop-link" href="<?=Url::to(["/user/default/courses"])?>">Мои курсы</a></li>
                            </ul>
                        </div>
                        <div class="header__login login"><a class="login__link" href="<?=Url::to(['/user/sign-in/logout'])?>">
                                <svg class="svg-sprite-icon icon-login">
                                    <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#login"></use>
                                </svg>
                                <div class="login__text">Выйти</div></a></div>
                    <?php endif; ?>


                    <div class="header__btns"><a class="btn   btn_default header__btn modal-btn" href="#modal-request">
                            <svg class="svg-sprite-icon icon-phone btn__icon_left btn__icon">
                                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#phone"></use>
                            </svg><span class="btn__label">Заказать звонок</span></a>
                        <div class="sandwich" data-target-id="site-overlay" data-target-class-toggle="is-active">
                            <div class="sandwich__line sandwich__line--top"></div>
                            <div class="sandwich__line sandwich__line--middle"></div>
                            <div class="sandwich__line sandwich__line--bottom"></div>
                        </div>
                    </div>
                </div>
                <div class="header__nav">
                    <nav class="menu">
                        <ul class="menu__list">
                            <li class="menu__item  "><a class="menu__link" href="<?=Url::to(["/courses"])?>">Обучение</a></li>
                            <li class="menu__item  "><a class="menu__link" href="<?=Url::to(["/consultations"])?>">Констультации</a>
                            </li>
                            <li class="menu__item  "><a class="menu__link" href="<?=Url::to(["/publications"])?>">Публикации</a>
                            </li>
                            <li class="menu__item  "><a class="menu__link" href="<?=Url::to(["/webinars"])?>">Вебинары</a>
                            </li>
                            <li class="menu__item  "><a class="menu__link" href="<?=Url::to(["/page/komu-eto-polezno"])?>">Кому это полезно</a>
                            </li>
                            <li class="menu__item  "><a class="menu__link" href="<?=Url::to(["/page/kak-prohodit-obucenie"])?>">Как проходит обучение</a>
                            </li>
                            <li class="menu__item  "><a class="menu__link" href="<?=Url::home()?>#reviews">Отзывы</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <div class="line_l_l line"></div>
    <div class="line_l line"></div>
    <div class="line_c line"></div>
    <div class="line_r line"></div>
    <div class="line_r_r line"></div>
    <?php echo $content ?>
        <footer class="footer">
            <div class="footer__wrapp">
                <div class="logo footer__logo"><a class="logo__link" href="/"><img class="logo__img" src="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/logo.png"></a></div>
                <ul class="contacts footer__contacts">
                    <li class="contacts__item contacts__item_mobile_sandwich"><a class="contacts__link" href="tel:+79896174572">
                            <div class="contacts__icon">
                                <svg class="svg-sprite-icon icon-phone">
                                    <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#phone"></use>
                                </svg>
                            </div>
                            <div class="contacts__text header__text">+79896174572</div></a>
                    </li>
                    <li class="contacts__item contacts__item_mobile_sandwich"><a class="contacts__link" href="tel:+380953942294">
                            <div class="contacts__icon">
                                <svg class="svg-sprite-icon icon-phone">
                                    <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#phone"></use>
                                </svg>
                            </div>
                            <div class="contacts__text header__text">+380953942294</div></a>
                    </li>
                    <li class="contacts__item">
                        <div class="contacts__icon">
                            <svg class="svg-sprite-icon icon-calendar">
                                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#calendar"></use>
                            </svg>
                        </div>
                        <div class="contacts__text header__text">с 9.00 до 19.00 по Мск</div>
                    </li>
                    <li class="contacts__item"><a class="contacts__link" href="mailto:astropsihology@mail.ua">
                            <div class="contacts__icon">
                                <svg class="svg-sprite-icon icon-envelop">
                                    <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#envelop"></use>
                                </svg>
                            </div>
                            <div class="contacts__text header__text">astropsihology@mail.ua</div></a>
                    </li>
                </ul>
                <div class="footer__nav">
                    <nav class="menu">
                        <ul class="menu__list">
                            <li class="menu__item  "><a class="menu__link" href="<?=Url::to(["/courses"])?>">Обучение</a></li>
                            <li class="menu__item  "><a class="menu__link" href="<?=Url::to(["/consultations"])?>">Констультации</a>
                            </li>
                            <li class="menu__item  "><a class="menu__link" href="<?=Url::to(["/publications"])?>">Публикации</a>
                            </li>
                            <li class="menu__item  "><a class="menu__link" href="<?=Url::to(["/webinars"])?>">Вебинары</a>
                            </li>
                            <li class="menu__item  "><a class="menu__link" href="<?=Url::to(["/page/komu-eto-polezno"])?>">Кому это полезно</a>
                            </li>
                            <li class="menu__item  "><a class="menu__link" href="<?=Url::to(["/page/kak-prohodit-obucenie"])?>">Как проходит обучение</a>
                            </li>
                            <li class="menu__item  "><a class="menu__link" href="<?=Url::home()?>#reviews">Отзывы</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <ul class="socials footer__socials">
                    <li class="socials__item"><a class="socials__link" href="whatsapp://send?phone=380953942294">
                            <svg class="svg-sprite-icon icon-whatsapp socials__icon">
                                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#whatsapp"></use>
                            </svg></a></li>
                    <li class="socials__item"><a class="socials__link" href="viber://add?number=380953942294">
                            <svg class="svg-sprite-icon icon-viber socials__icon">
                                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#viber"></use>
                            </svg></a></li>
                    <li class="socials__item"><a class="socials__link" href="https://t.me/karina_dem4enko">
                            <svg class="svg-sprite-icon icon-telegram socials__icon">
                                <use xlink:href="<?= Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#telegram"></use>
                            </svg></a></li>
                </ul>
            </div>
        </footer>
		
		
	
		
		
        <div class="form form_modal" id="modal-request" >
		<?php
		$model = new OrderForm();
		$form = ActiveForm::begin([
					'id' => 'order-form',
					'action' => Url::to('/form/order-save'),
					'enableAjaxValidation' => true,
					'validationUrl' =>  Url::to('/form/order-validate'),
				]); ?>
            <div class="form__header">
                <div class="form__title">Заполните заявку</div>
                <div class="form__subtitle">и наши специалисты свяжуться с вами</div>
            </div>
            <div class="form__body">
                <dl class="form-cell form-cell_half_width">
                    <dt class="form-cell__hline">
                        <label for="Имя">Имя</label>
                    </dt>
                    <dd class="form-cell__field-wrapper">
						<?= $form->field($model, 'name')->textInput(['class' => 'form-cell__field', 'id'=>'callback-name', 'placeholder'=>'Имя'])->label(false) ?>
						<?= $form->field($model, 'last_name')->hiddenInput(['value' => '-'])->label(false) ?>
						<?= $form->field($model, 'email')->hiddenInput(['value' => 'ooo@oo.oo'])->label(false) ?>
                    </dd>
                </dl>
                <dl class="form-cell form-cell_half_width">
                    <dt class="form-cell__hline">
                        <label for="Телефон">Телефон</label>
                    </dt>
                    <dd class="form-cell__field-wrapper">
                       <?= $form->field($model, 'phone')->textInput(['class' => 'form-cell__field', 'id'=>'callback-tel', 'placeholder'=>'+7 (900) 900 90 90', 'type'=>'tel'])->label(false) ?>
                    </dd>
                </dl>
            </div>
            <div class="form__policy">Нажимая на кнопку, вы даете согласие на обработку своих персональных данных <br><a href="#">Политика конфиденциальности</a></div>
            <div class="form__btn-wrapp">
                <button class="btn  btn_default form__btn" type="submit"><span class="btn__label">Отправить</span>
                </button>
            </div>
			<?php ActiveForm::end(); ?>
        </div>
        <form class="form form_modal" id="modal-registration" action="<?=Url::to(['/user/sign-in/signup'])?>" method="post">
            <div class="form__header">
                <div class="form__title">Зарегистрируйтесь</div>
            </div>
            <div class="form__body">
                <dl class="form-cell form-cell_half_width">
                    <dt class="form-cell__hline">
                        <label for="Email">Email</label>
                    </dt>
                    <dd class="form-cell__field-wrapper">
                        <input class="form-cell__field required " type="email" id="Email" name="SignupForm[email]" placeholder="Email"/>
                    </dd>
                </dl>
                <dl class="form-cell form-cell_half_width">
                    <dt class="form-cell__hline">
                        <label for="Имя">Имя</label>
                    </dt>
                    <dd class="form-cell__field-wrapper">
                        <input class="form-cell__field required " type="text" id="Имя" name="SignupForm[name]" placeholder="Имя"/>
                    </dd>
                </dl>
                <dl class="form-cell form-cell_half_width">
                    <dt class="form-cell__hline">
                        <label for="pass">Пароль</label>
                    </dt>
                    <dd class="form-cell__field-wrapper form-cell__field-wrapper_footnote"><span class="form-cell__toggle is-active" onclick="togglePass(event)"></span>
                        <input class="form-cell__field required" type="password" name="SignupForm[password]" id="pass" placeholder="Пароль" value="">
                    </dd>
                </dl>
                <dl class="form-cell form-cell_half_width">
                    <dt class="form-cell__hline">
                        <label for="pass_confirm">Повторите пароль</label>
                    </dt>
                    <dd class="form-cell__field-wrapper"><span class="form-cell__toggle is-active" onclick="togglePass(event)"></span>
                        <input class="form-cell__field required" type="password" name="SignupForm[password_confirm]" id="pass_confirm" placeholder="Пароль" value="">
                    </dd>
                </dl>
                <dl class="form-cell form-cell_half_width">
                    <dt class="form-cell__hline">
                        <label for="Телефон">Телефон</label>
                    </dt>
                    <dd class="form-cell__field-wrapper">
                        <input class="form-cell__field required " type="text" id="Телефон" name="SignupForm[phone]" placeholder="+_ (___) ___-__-__"/>
                    </dd>
                </dl>
            </div>
            <div class="form__btn-wrapp">
                <button class="btn  btn_default form__btn" type="submit" style="margin-top: 2rem;"><span class="btn__label">Зарегистрироваться</span>
                </button>
            </div>
            <div class="form__policy">У вас уже есть аккаунт? <br><a class="modal-btn" href="#modal-login">Войти</a></div>
        </form>
        <form class="form form_modal" id="modal-login" action="<?=Url::to(['/user/sign-in/login'])?>" method="post">
            <div class="form__header">
                <div class="form__title">Вход в аккаунт</div>
            </div>
            <div class="form__body">
                <dl class="form-cell form-cell_half_width">
                    <dt class="form-cell__hline">
                        <label for="Почта">Почта</label>
                    </dt>
                    <dd class="form-cell__field-wrapper">
                        <input class="form-cell__field required " type="email" id="Почта" name="LoginForm[identity]" placeholder="Ваша почта"/>
                    </dd>
                </dl>
                <dl class="form-cell form-cell_half_width">
                    <dt class="form-cell__hline">
                        <label for="pass">Пароль</label>
                    </dt>
                    <dd class="form-cell__field-wrapper"><span class="form-cell__toggle is-active" onclick="togglePass(event)"></span>
                        <input class="form-cell__field required" type="password" name="LoginForm[password]" id="pass" placeholder="Пароль" value="">
                    </dd>
                </dl>
            </div>
            <!--<div class="form__policy"><a href="#">Восстановить пароль</a></div>-->
            <div class="form__btn-wrapp">
                <button class="btn  btn_default form__btn" type="submit"><span class="btn__label">Войти</span>
                </button>
            </div>
            <div class="form__policy">У вас еще нет аккаунта? <br><a class="modal-btn" href="#modal-registration">Зарегистрироваться</a></div>
        </form>
		<div class="form form_modal" id="modal-review" >
		<?php
		$model = new Review();
		$form = ActiveForm::begin([
					'id' => 'review-form',
					'action' => Url::to('/form/review-save'),
					'enableAjaxValidation' => true,
					'validationUrl' =>  Url::to('/form/review-validate'),
				]); ?>
            <div class="form__header">
                <div class="form__title">Оставьте отзыв</div>
            </div>
            <div class="form__body">
                <dl class="form-cell form-cell_third_width">
                    <dt class="form-cell__hline">
                        <label for="Имя">Имя</label>
                    </dt>
                    <dd class="form-cell__field-wrapper">
						<?= $form->field($model, 'name')->textInput(['class' => 'form-cell__field', 'placeholder'=>'Имя'])->label(false) ?>
						<?= $form->field($model, 'date')->hiddenInput(['value' => date("j-n-Y")])->label(false) ?>
                    </dd>
                </dl>
                <dl class="form-cell form-cell_third_width">
                    <dt class="form-cell__hline">
                        <label for="Телефон">Телефон</label>
                    </dt>
                    <dd class="form-cell__field-wrapper">
						<?= $form->field($model, 'phone')->textInput(['class' => 'form-cell__field', 'id'=>'callback-tel', 'placeholder'=>'+7 (900) 900 90 90', 'type'=>'tel'])->label(false) ?>
                    </dd>
                </dl>
                <dl class="form-cell form-cell_third_width">
                    <dt class="form-cell__hline">
                        <label for="Почта">Почта</label>
                    </dt>
                    <dd class="form-cell__field-wrapper">
						<?= $form->field($model, 'email')->textInput(['class' => 'form-cell__field', 'placeholder'=>'Ваша почта'])->label(false) ?>
                    </dd>
                </dl>
                <dl class="form-cell form-cell_full_width">
                    <dt class="form-cell__hline">
                        <label for="Отзыв">Отзыв</label>
                    </dt>
                    <dd class="form-cell__field-wrapper">
						<?= $form->field($model, 'comment')->textInput(['class' => 'form-cell__field', 'placeholder'=>'Отзыв'])->label(false) ?>
                    </dd>
                </dl>
            </div>
            <div class="form__policy">Нажимая на кнопку, вы даете согласие на обработку своих персональных данных <br><a href="#">Политика конфиденциальности</a></div>
            <div class="form__btn-wrapp">
                <button class="btn  btn_default form__btn" type="submit"><span class="btn__label">Отправить</span>
                </button>
            </div>
			<?php ActiveForm::end(); ?>
        </div>
    </div>

<div id="toTop"></div>
<?php $this->endContent() ?>