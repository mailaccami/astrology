<?php

use frontend\widgets\PaymentButton\PaymentButton;

/**
 * @var \common\models\Course $courses
 * @var \common\models\Course $course
 * @var \common\models\Category $categorie
 * @var \common\models\Category $categories
 */

$this->title = "Все курсы";

?>

<div class="content">
    <div id="scene">
        <svg class="svg-sprite-icon icon-zodiac-1" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-1"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-2" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-2"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-3" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-3"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-4" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-4"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-5" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-5"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-6" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-6"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-7" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-7"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-8" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-8"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-9" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-9"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-10" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-10"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-11" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-11"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-12" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-12"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-13" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-13"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-14" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-14"></use>
        </svg>
        <svg class="svg-sprite-icon icon-zodiac-15" data-depth="0.1">
            <use xlink:href="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/svg/symbol/sprite.svg#zodiac-15"></use>
        </svg><img class="constellation-1" src="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-1.png" data-depth="0.3"><img class="constellation-2" src="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-2.png" data-depth="0.4"><img class="constellation-3" src="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-3.png" data-depth="0.5"><img class="constellation-4" src="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-4.png" data-depth="0.6"><img class="constellation-5" src="<?=Yii::getAlias('@web') ?>/theme/astrology/astrology-frontend/static/images/general/constellation-5.png" data-depth="0.7">
    </div>
    <section class="account-courses">
        <div class="container">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                </li>
                <li class="breadcrumbs__item"><span class="breadcrumbs__text">Курсы</span>
                </li>
            </ul>
            <div class="title">Все курсы</div>
            <ul class="tabs-nav">

                <?php $i=0; foreach ($categories as $categorie): $i++;?>
                    <li class="tabs-nav__item btn <?php if($i==1):?> btn_default tabs-nav__item--active is-active<?php else:?>btn_transparent<?php endif;?>" data-tab-name="tab-<?=$i?>" data-aos="zoom-out" data-aos-delay="<?=$i*50?>"><?=$categorie->name?></li>
                <?php endforeach; ?>

            </ul>
            <div class="tabs-content">
                <?php $i=0; foreach ($categories as $categorie): $i++;?>
                    <div class="tabs-content__item tab-<?=$i?> <?php if($i==1):?>tabs-content__item--active is-active<?php endif;?>">
                        <div class="cards-grid">
                            <?php $ic= 0; foreach ($categorie->courses as $course): $ic++;?>
                            <div class="card" data-aos="flip-left" data-aos-delay="50">
                                <div class="card__header">
                                    <div class="card__ribbon">
                                        <div class="card__counter"><?php if($ic < 10) { echo '0';} echo $ic; ?></div>
                                    </div><a class="card__video" >
                                        <img class="card__img" src="<?=$course->image?>"/></a>
                                </div>
                                <div class="card__body">
                                    <div class="card__title subtitle"><?=$course->title?></div>
                                    <div class="card__content"><?=$course->short_content?></div><a class="btn   btn_transparent card__btn" href="<?=\yii\helpers\Url::to(['/courses/view', 'slug'=>$course->slug])?>"><span class="btn__label">Перейти к курсу</span></a>
                                </div>
                            </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

        </div>
    </section>
</div>
