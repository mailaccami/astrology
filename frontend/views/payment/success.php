<?php
use yii\bootstrap\BootstrapAsset;
BootstrapAsset::register($this);
?>

<div class="row">
    <div class="container">
        <div id="w2" class="alert-success alert alert-dismissible" role="alert">
            Заказ успешно оплачен!
        </div>
    </div>
</div>