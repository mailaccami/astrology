<?php
use yii\bootstrap\BootstrapAsset;
BootstrapAsset::register($this);
?>

<div class="row">
    <div class="container">
        <div id="w2" class="alert-danger alert alert-dismissible" role="alert">
            При оплате заказа произошла ошибка, повторите позже.
        </div>
    </div>
</div>