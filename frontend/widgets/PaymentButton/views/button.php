<?php

use yii\helpers\Url;

?>
<?php if(Yii::$app->user->isGuest): ?>
    <a class="item__btn btn-green popup-link" onclick="<?=$id?>();" href="#login"><?=$button_text?></a>
<?php else: ?>
    <script>
        function <?=$id?>() {
            var widget = new cp.CloudPayments({language: 'ru-RU'});
            widget.charge(
                {
                    "publicId":"pk_f8b219593fcb4f6783a5539388b79",
                    "description":"Оплата курса: <?=$course->title?>",
                    "amount":<?=$course->price?>,
                    "currency":"RUB",
                    "email":"<?=Yii::$app->user->identity->email?>",
                    "skin":"classic",
                    "data":{
                        "course_id": "<?=$course->id?>",
                        "user_id": "<?=Yii::$app->user->identity->id?>"
                    }
                },
                '<?=Url::to(["/payment/success"])?>',
                '<?=Url::to(["/payment/fail"])?>'
            );
        }
    </script>
    <a class="item__btn btn-green" onclick="<?=$id?>();" href="#"><?=$button_text?></a>
<?php endif; ?>
