<?php

namespace frontend\widgets\PaymentButton;

use yii\base\Widget;

class PaymentButton extends Widget
{
    public $course;
    public $button_text;

    public function run()
    {
        $id = "button_handler_".uniqid();

        return $this->render('button', [
            'id' => $id,
            'course' => $this->course,
            'button_text' => $this->button_text,
        ]);
    }
}
