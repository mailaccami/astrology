<?php

use Sitemaped\Sitemap;

return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        // Pages
        ['pattern' => 'page/<slug>', 'route' => 'page/view'],

        ['pattern' => 'consultations', 'route' => 'site/consultations'],
        ['pattern' => 'consultations/<slug>', 'route' => 'site/consultations-view'],

        ['pattern' => 'publications', 'route' => 'site/publications'],
        ['pattern' => 'publications/<slug>', 'route' => 'site/publications-view'],

        ['pattern' => 'webinars', 'route' => 'site/webinars'],
        ['pattern' => 'webinars/<slug>', 'route' => 'site/webinars-view'],

        // Articles
        ['pattern' => 'article/index', 'route' => 'article/index'],
        ['pattern' => 'article/attachment-download', 'route' => 'article/attachment-download'],
        ['pattern' => 'article/<slug>', 'route' => 'article/view'],

        // Courses
        ['pattern' => 'courses/module', 'route' => 'courses/module'],
        ['pattern' => 'courses/<slug>', 'route' => 'courses/view'],
        ['pattern' => 'user/courses', 'route' => 'user/default/courses'],
        ['pattern' => 'user/course/<slug>', 'route' => 'user/default/course'],
        ['pattern' => 'user/course/<course>/module/<module>/lesson/<id>', 'route' => 'user/default/lesson'],

        // Sitemap
        ['pattern' => 'sitemap.xml', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_XML]],
        ['pattern' => 'sitemap.txt', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_TXT]],
        ['pattern' => 'sitemap.xml.gz', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_XML, 'gzip' => true]],
    ]
];
