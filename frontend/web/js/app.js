$( document ).ready(function() {

    $("#modal-login").validate();

    $("#modal-login").submit(function () {
        if($(this).valid()) {
            let data = $(this).serialize();
            let url = $(this).attr("action");

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: data
            }).done(function( errors ) {
                if(errors.length == 0) document.location.reload();
                for (var key in errors) {
                    alert(errors[key]);
                }
            });

        }
        return false;
    });

    $("#modal-registration").validate({
        rules: {
            "SignupForm[phone]": {
                required: true,
            }
        },
    });

    $("#modal-registration").submit(function () {
        if($(this).valid()) {
            let data = $(this).serialize();
            let url = $(this).attr("action");

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: data
            }).done(function( errors ) {
                if(errors.length == 0) document.location.reload();
                for (var key in errors) {
                    alert(errors[key]);
                }
            });

        }
        return false;
    });

    $('#order-form').on('beforeSubmit', function () {
        var $yiiform = $(this);
        // отправляем данные на сервер
        $.ajax({
                type: $yiiform.attr('method'),
                url: $yiiform.attr('action'),
                data: $yiiform.serializeArray()
            }
        )
        .done(function(data) {
            if(data.success) {
                $yiiform.html("<span class='message'><b>Спасибо!</b><br> Наш менеджер свяжется с Вами в ближайшее время.</span>");
            }
        })

        return false; // отменяем отправку данных формы
    })
    $('#review-form').on('beforeSubmit', function () {
        var $yiiform = $(this);
        // отправляем данные на сервер
        $.ajax({
                type: $yiiform.attr('method'),
                url: $yiiform.attr('action'),
                data: $yiiform.serializeArray()
            }
        )
        .done(function(data) {
            if(data.success) {
                $yiiform.html("<span class='message'><b>Спасибо!</b></span>");
            }
        })

        return false; // отменяем отправку данных формы
    })
    $('#question-form').on('beforeSubmit', function () {
        var $yiiform = $(this);
        // отправляем данные на сервер
        $.ajax({
                type: $yiiform.attr('method'),
                url: $yiiform.attr('action'),
                data: $yiiform.serializeArray()
            }
        )
        .done(function(data) {
            if(data.success) {
                $yiiform.html("<span class='message'><b>Спасибо!</b><br> Наш менеджер свяжется с Вами в ближайшее время.</span>");
            }
        })

        return false; // отменяем отправку данных формы
    })
    $('#homework-form').on('beforeSubmit', function () {
        var $yiiform = $(this);
        // отправляем данные на сервер
        $.ajax({
                type: $yiiform.attr('method'),
                url: $yiiform.attr('action'),
                data: $yiiform.serializeArray()
            }
        )
        .done(function(data) {
            if(data.success) {
                $yiiform.html("<span class='message'><b>Задание отправлено!</b><br> Куратор свяжется с Вами в ближайшее время.</span>");
            }
        })

        return false; // отменяем отправку данных формы
    })

});