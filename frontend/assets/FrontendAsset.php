<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use common\assets\Html5shiv;
use rmrevin\yii\fontawesome\NpmFreeAssetBundle;
use yii\bootstrap4\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;
use trntv\filekit\widget\UploadAsset;

/**
 * Frontend application asset
 */
class FrontendAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';
    /**
     * @var string
     */
    public $baseUrl = '@web';

    /**
     * @var array
     */
    public $css = [
        'theme/astrology/astrology-frontend/static/css/styles.min.css',
        'css/app.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/maskedinput/jquery.maskedinput.min.js',
        'theme/astrology/astrology-frontend/static/js/libs.min.js',
        'theme/astrology/astrology-frontend/static/js/main.js',
        'js/app.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        Html5shiv::class,
        'yii\web\JqueryAsset',
    ];
}
