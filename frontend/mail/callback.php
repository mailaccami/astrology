<?php

use yii\helpers\Html;

?>
<div class="container" style="width:700px;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;">
    <table class="table-order-info" cellpadding="0" cellspacing="0" border="0" width="100%">

        <tr>
            <td style="width:350px;"><span class="title"
                                           style="vertical-align:middle;font-family:'Arial', 'Tahoma', 'Geneva', sans-serif;font-weight:700;font-size:15px;line-height:2;background-color:#8500ff;color:#ffffff;display:inline-block;padding-top:0;padding-bottom:0;padding-right:15px;padding-left:15px;">Заказать обратный звонок:</span>
            </td>
            <td style="width:350px;"><span class="subtitle order-type"
                                           style="vertical-align:middle;font-family:'Arial', 'Tahoma', 'Geneva', sans-serif;font-weight:600;font-size:18px;line-height:1.4;"><?= $data['name'] ?></span>
            </td>
            <td style="width:350px;"><span class="subtitle order-type"
                                           style="vertical-align:middle;font-family:'Arial', 'Tahoma', 'Geneva', sans-serif;font-weight:600;font-size:18px;line-height:1.4;"><?= $data['phone'] ?></span>
            </td>
        </tr>

    </table>

</div>
