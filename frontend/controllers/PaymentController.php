<?php

namespace frontend\controllers;

use yii\web\Controller;
use Yii;
use common\models\UserCourse;

/**
 * Site controller
 */
class PaymentController extends Controller
{

    /**
     * @return string
     */
    public function actionFail()
    {
        return $this->render('fail');
    }

    /**
     * @return string
     */
    public function actionSuccess()
    {
        return $this->render('success');
    }
    /**
     * @return string
     */
    public function actionAviso()
    {
        $post = $_GET;

        //Yii::error($post, 'payment_fail'); //запись в лог
        //Yii::error($post['Status'], 'payment_fail'); //запись в лог

        if($post['Status'] == 'Completed') {
            $user_course = new UserCourse();
            $user_course->course_id = json_decode($post['Data'])->course_id;
            $user_course->user_id = json_decode($post['Data'])->user_id;
            $result = $user_course->save();
        }

        return $this->render('success');
    }

}
