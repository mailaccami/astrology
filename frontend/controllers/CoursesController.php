<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\Course;
use common\models\Module;
use yii\web\Controller;

/**
 * Site controller
 */
class CoursesController extends Controller
{

    /**
     * @return string
     */
    public function actionIndex()
    {

        $courses = Course::find()->orderBy(['weight' => SORT_ASC])->all();
        $categories = Category::find()->all();

        return $this->render('index', [
            "courses" => $courses,
            "categories" => $categories,
        ]);
    }

    public function actionView($slug)
    {
        $model = Course::find()->where(['slug' => $slug])->one();
        if (!$model) {
            throw new NotFoundHttpException;
        }

        return $this->render("view", [
            'model' => $model,
        ]);
    }
    public function actionModule($id)
    {
        $model = Module::find()->where(['id' => $id])->one();
        if (!$model) {
            throw new NotFoundHttpException;
        }

        return $this->render("module", [
            'model' => $model,
        ]);
    }

}
