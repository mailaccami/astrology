<?php

namespace frontend\controllers;

use common\models\OrderForm;
use common\models\QuestionForm;
use common\models\Review;
use common\models\HomeworkForm;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\swiftmailer\Mailer;

class FormController extends Controller
{
    public function actionOrderValidate()
    {

        $model = new OrderForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    public function actionOrderSave()
    {
        $model = new OrderForm();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
			
			$message = Yii::$app->mailer->compose(['html'=>'callback'], [
                'data' => $model,
            ]);

            $message->setFrom(["andpopov3@yandex.ru" => "Admin"])->setTo(["mailaccami@gmail.com"])->setSubject('Заказать обратный звонок')->send();

			
            return ['success' => $model->save()];
        }
    }

    public function actionQuestionValidate()
    {

        $model = new QuestionForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
    public function actionReviewValidate()
    {

        $model = new Review();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    public function actionQuestionSave()
    {
        $model = new QuestionForm();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => $model->save()];
        }
    }
	
	public function actionReviewSave()
    {
        $model = new Review();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => $model->save()];
        }
    }
	
    public function actionHomeworkValidate()
    {

        $model = new HomeworkForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    public function actionHomeworkSave()
    {
        $model = new HomeworkForm();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => $model->save()];
        }
    }
}