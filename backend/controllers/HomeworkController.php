<?php

namespace backend\controllers;

use common\models\Module;
use common\models\Advanture;
use common\models\WhatLearh;
use common\models\Who;
use common\models\HomeworkForm;
use Yii;
use common\models\Course;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
/**
 * CourseController implements the CRUD actions for Course model.
 */
class HomeworkController extends Controller
{


    /**
     * Lists all Course models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => HomeworkForm::find()->where(['teacher_id'=>Yii::$app->user->identity->id]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Course model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Course model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Course();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Course model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        $dataAdvantureProvider = new ActiveDataProvider([
            'query' => Advanture::find()->orderBy("weight")->where(['course_id' => $id]),
        ]);
        $dataWhoProvider = new ActiveDataProvider([
            'query' => Who::find()->orderBy("weight")->where(['course_id' => $id]),
        ]);
        $dataWhatLearhProvider = new ActiveDataProvider([
            'query' => WhatLearh::find()->orderBy("weight")->where(['course_id' => $id]),
        ]);
        $dataModuleProvider = new ActiveDataProvider([
            'query' => Module::find()->orderBy("weight")->where(['course_id' => $id]),
        ]);

        return $this->render('update', [
            'model' => $model,
            'dataAdvantureProvider' => $dataAdvantureProvider,
            'dataWhoProvider' => $dataWhoProvider,
            'dataWhatLearhProvider' => $dataWhatLearhProvider,
            'dataModuleProvider' => $dataModuleProvider,
        ]);
    }

    /**
     * Deletes an existing Course model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HomeworkForm::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
