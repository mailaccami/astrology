<?php

namespace backend\controllers;

use himiklab\sortablegrid\SortableGridAction;
use Yii;
use common\models\Who;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WhoController implements the CRUD actions for Who model.
 */
class WhoController extends Controller
{

    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Who::className(),
            ],
        ];
    }

    /**
     * Lists all Who models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Who::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Who model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Who model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($course_id)
    {
        $model = new Who();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['course/update', 'id' => $course_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'course_id' => $course_id
        ]);
    }

    /**
     * Updates an existing Who model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $course_id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['course/update', 'id' => $course_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'course_id' => $course_id,
        ]);
    }

    /**
     * Deletes an existing Who model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $course_id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['course/update', 'id' => $course_id]);
    }

    /**
     * Finds the Who model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Who the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Who::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
