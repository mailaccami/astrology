<?php

use yii\helpers\Html;
use yii\grid\GridView;
use himiklab\sortablegrid\SortableGridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Курсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-index">

    <p>
        <?= Html::a('Создать курс', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'title',
            //'description:ntext',
            //'thumbnail_base_url:url',
            //'thumbnail_path',
            //'header',
            //'lasting',
            //'price',
            //'teacher_name',
            //'teacher_base_url:url',
            //'teacher_path',
            //'teacher_description',
            //'teacher_specialization',
            //'teacher_id',

            [
                'class' => \common\widgets\ActionColumn::class,
                'options' => ['style' => 'width: 5%'],
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>


</div>
