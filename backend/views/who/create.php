<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Advanture */

$this->title = 'Создать "Кому подходит"';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advanture-create">

    <?= $this->render('_form', [
        'model' => $model,
        'course_id' => $course_id
    ]) ?>

</div>
