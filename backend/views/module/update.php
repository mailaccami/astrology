<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Advanture */

$this->title = 'Редактировать модуль: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="advanture-update">

    <?= $this->render('_form', [
        'model' => $model,
        'course_id' => $course_id,
        'dataLessonProvider' => $dataLessonProvider,
    ]) ?>

</div>
