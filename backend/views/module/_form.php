<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Course;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;
use himiklab\sortablegrid\SortableGridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Advanture */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advanture-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'thumbnail')->widget(
        Upload::class,
        [
            'url' => ['/file/storage/upload'],
            'maxFileSize' => 5000000, // 5 MiB,
            'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
        ]
    ) ?>

    <?php echo $form->field($model, 'short_content')->widget(
        \yii\imperavi\Widget::class,
        [
            'plugins' => ['fullscreen', 'fontcolor', 'video'],
            'options' => [
                'minHeight' => 400,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => true,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file/storage/upload-imperavi']),
            ],
        ]
    ) ?>
    <?php echo $form->field($model, 'description')->widget(
        \yii\imperavi\Widget::class,
        [
            'plugins' => ['fullscreen', 'fontcolor', 'video'],
            'options' => [
                'minHeight' => 400,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => true,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file/storage/upload-imperavi']),
            ],
        ]
    ) ?>

    <?php if($model->id): ?>
        <?php $module = $model;?>
        <div class="card">
            <div class="card-body">
                <h5 style="display:block;margin-bottom: 10px" class="card-title">Уроки</h5>
                <div class="card-text">
                    <?= SortableGridView::widget([
                        'dataProvider' => $dataLessonProvider,
                        'sortableAction' => '/lesson/sort',
                        'columns' => [

                            'title',

                            [
                                'class' => \common\widgets\ActionColumn::class,
                                'options' => ['style' => 'width: 5%'],
                                'template' => '{update} {delete}',
                                'controller' => 'lesson',
                                'buttons' => [
                                    'update' => function ($url, $model) use ($module,$course_id) {
                                        $url .= '&module_id=' . $module->id.'&course_id=' . $course_id; //This is where I want to append the $lastAddress variable.
                                        return Html::a('<i class="fa-fw fas fa-edit" aria-hidden=""></i>', $url);
                                    },
                                    'delete' => function ($url, $model) use ($module,$course_id) {
                                        $url .= '&module_id=' . $module->id.'&course_id=' . $course_id; //This is where I want to append the $lastAddress variable.
                                        return Html::a('<i class="fa-fw fas fa-trash" aria-hidden=""></i>', $url);
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
                <a href="<?=Url::to(['lesson/create', 'module_id'=>$model->id,'course_id'=>$course_id])?>" class="card-link">Создать</a>
            </div>
        </div>
    <?php endif; ?>

    <?php $model->course_id = $course_id;?>

    <?php echo $form->field($model, 'course_id')->dropDownList(\yii\helpers\ArrayHelper::map(
        Course::find()->asArray()->all(),
        'id',
        'title'
    ), ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
