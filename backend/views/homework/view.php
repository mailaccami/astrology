<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Course */

$this->title = $model->lesson_name;
$this->params['breadcrumbs'][] = ['label' => 'Домашние задания', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="course-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'lesson_name',
            [
                'label' => 'Учитель',
                'value' => $model->teacher->name,
            ],
            [
                'label' => 'Ученик имя',
                'value' => $model->user->name,
            ],
            [
                'label' => 'Ученик почта',
                'value' => $model->user->email,
            ],
            'message',
            [
                'label' => 'Файл',
                'format' => 'raw',
                'value' => '<a href="'.$model->file1.'" target="_blank" download="download">Скачать файл</a>',
            ],

        ],
    ]) ?>

</div>
