<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;
use yii\helpers\Url;
use himiklab\sortablegrid\SortableGridView;

/* @var $this yii\web\View */
/* @var $model common\models\Course */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'slug')
        ->hint(Yii::t('backend', 'If you leave this field empty, the slug will be generated automatically'))
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'header')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lasting')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?php if($model->id): ?>
        <?php $course = $model;?>
        <div class="card">
            <div class="card-body">
                <h5 style="display:block;margin-bottom: 10px" class="card-title">Преимущества</h5>
                <div class="card-text">
                    <?= SortableGridView::widget([
                        'dataProvider' => $dataAdvantureProvider,
                        'sortableAction' => '/advanture/sort',
                        'columns' => [

                            'title',

                            [
                                'class' => \common\widgets\ActionColumn::class,
                                'options' => ['style' => 'width: 5%'],
                                'template' => '{update} {delete}',
                                'controller' => 'advanture',
                                'buttons' => [
                                    'update' => function ($url, $model) use ($course) {
                                        $url .= '&course_id=' . $course->id; //This is where I want to append the $lastAddress variable.
                                        return Html::a('<i class="fa-fw fas fa-edit" aria-hidden=""></i>', $url);
                                    },
                                    'delete' => function ($url, $model) use ($course) {
                                        $url .= '&course_id=' . $course->id; //This is where I want to append the $lastAddress variable.
                                        return Html::a('<i class="fa-fw fas fa-trash" aria-hidden=""></i>', $url);
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
                <a href="<?=Url::to(['advanture/create', 'course_id'=>$model->id])?>" class="card-link">Создать</a>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 style="display:block;margin-bottom: 10px" class="card-title">Кому подходит</h5>
                <div class="card-text">
                    <?= SortableGridView::widget([
                        'dataProvider' => $dataWhoProvider,
                        'sortableAction' => '/who/sort',
                        'columns' => [

                            'title',

                            [
                                'class' => \common\widgets\ActionColumn::class,
                                'options' => ['style' => 'width: 5%'],
                                'template' => '{update} {delete}',
                                'controller' => 'who',
                                'buttons' => [
                                    'update' => function ($url, $model) use ($course) {
                                        $url .= '&course_id=' . $course->id; //This is where I want to append the $lastAddress variable.
                                        return Html::a('<i class="fa-fw fas fa-edit" aria-hidden=""></i>', $url);
                                    },
                                    'delete' => function ($url, $model) use ($course) {
                                        $url .= '&course_id=' . $course->id; //This is where I want to append the $lastAddress variable.
                                        return Html::a('<i class="fa-fw fas fa-trash" aria-hidden=""></i>', $url);
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
                <a href="<?=Url::to(['who/create', 'course_id'=>$model->id])?>" class="card-link">Создать</a>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 style="display:block;margin-bottom: 10px" class="card-title">Чему вы научитесь</h5>
                <div class="card-text">
                    <?= SortableGridView::widget([
                        'dataProvider' => $dataWhatLearhProvider,
                        'sortableAction' => '/what-learh/sort',
                        'columns' => [

                            'title',

                            [
                                'class' => \common\widgets\ActionColumn::class,
                                'options' => ['style' => 'width: 5%'],
                                'template' => '{update} {delete}',
                                'controller' => 'what-learh',
                                'buttons' => [
                                    'update' => function ($url, $model) use ($course) {
                                        $url .= '&course_id=' . $course->id; //This is where I want to append the $lastAddress variable.
                                        return Html::a('<i class="fa-fw fas fa-edit" aria-hidden=""></i>', $url);
                                    },
                                    'delete' => function ($url, $model) use ($course) {
                                        $url .= '&course_id=' . $course->id; //This is where I want to append the $lastAddress variable.
                                        return Html::a('<i class="fa-fw fas fa-trash" aria-hidden=""></i>', $url);
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
                <a href="<?=Url::to(['what-learh/create', 'course_id'=>$model->id])?>" class="card-link">Создать</a>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 style="display:block;margin-bottom: 10px" class="card-title">Модули</h5>
                <div class="card-text">
                    <?= SortableGridView::widget([
                        'dataProvider' => $dataModuleProvider,
                        'sortableAction' => '/module/sort',
                        'columns' => [

                            'title',

                            [
                                'class' => \common\widgets\ActionColumn::class,
                                'options' => ['style' => 'width: 5%'],
                                'template' => '{update} {delete}',
                                'controller' => 'module',
                                'buttons' => [
                                    'update' => function ($url, $model) use ($course) {
                                        $url .= '&course_id=' . $course->id; //This is where I want to append the $lastAddress variable.
                                        return Html::a('<i class="fa-fw fas fa-edit" aria-hidden=""></i>', $url);
                                    },
                                    'delete' => function ($url, $model) use ($course) {
                                        $url .= '&course_id=' . $course->id; //This is where I want to append the $lastAddress variable.
                                        return Html::a('<i class="fa-fw fas fa-trash" aria-hidden=""></i>', $url);
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
                <a href="<?=Url::to(['module/create', 'course_id'=>$model->id])?>" class="card-link">Создать</a>
            </div>
        </div>
    <?php endif; ?>

    <?php echo $form->field($model, 'description')->widget(
        \yii\imperavi\Widget::class,
        [
            'plugins' => ['fullscreen', 'fontcolor', 'video'],
            'options' => [
                'minHeight' => 400,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => true,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file/storage/upload-imperavi']),
            ],
        ]
    ) ?>

    <?php echo $form->field($model, 'thumbnail')->widget(
        Upload::class,
        [
            'url' => ['/file/storage/upload'],
            'maxFileSize' => 5000000, // 5 MiB,
            'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
        ]
    ) ?>

    <?= $form->field($model, 'teacher_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'teacher_specialization')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'teacher_description')->widget(
        \yii\imperavi\Widget::class,
        [
            'plugins' => ['fullscreen', 'fontcolor', 'video'],
            'options' => [
                'minHeight' => 400,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => true,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file/storage/upload-imperavi']),
            ],
        ]
    ) ?>

    <?php echo $form->field($model, 'teacher_img')->widget(
        Upload::class,
        [
            'url' => ['/file/storage/upload'],
            'maxFileSize' => 5000000, // 5 MiB,
            'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
        ]
    ) ?>


    <?php echo $form->field($model, 'teacher_id')->dropDownList(\yii\helpers\ArrayHelper::map(
        User::find()->asArray()->all(),
        'id',
        'name'
    ), ['prompt' => '']) ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
