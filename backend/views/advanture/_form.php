<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Course;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;

/* @var $this yii\web\View */
/* @var $model common\models\Advanture */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advanture-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'description')->widget(
        \yii\imperavi\Widget::class,
        [
            'plugins' => ['fullscreen', 'fontcolor', 'video'],
            'options' => [
                'minHeight' => 400,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => true,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file/storage/upload-imperavi']),
            ],
        ]
    ) ?>

    <?php echo $form->field($model, 'thumbnail')->widget(
        Upload::class,
        [
            'url' => ['/file/storage/upload'],
            'maxFileSize' => 5000000, // 5 MiB,
            'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png|svg)$/i'),
        ]
    ) ?>

    <?php $model->course_id = $course_id;?>

    <?php echo $form->field($model, 'course_id')->dropDownList(\yii\helpers\ArrayHelper::map(
        Course::find()->asArray()->all(),
        'id',
        'title'
    ), ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
