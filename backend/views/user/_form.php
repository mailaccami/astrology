<?php

use common\models\User;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\UserForm */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $roles yii\rbac\Role[] */
/* @var $permissions yii\rbac\Permission[] */
?>

<div class="user-form">
    <?php $form = ActiveForm::begin() ?>
        <div class="card">
            <div class="card-body">
                <?php echo $form->field($model, 'email') ?>
                <?php echo $form->field($model, 'name') ?>
                <?php echo $form->field($model, 'telegram') ?>
                <?php echo $form->field($model, 'show_main')->radioList([
                        1 => "Выводить",
                        0 => "Не выводить",
                ]) ?>
                <?php echo $form->field($model, 'courses_ids')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\common\models\Course::find()->orderBy("weight")->all(), 'id', function($model) {
                        /** @var Product $model */
                        return $model->title;
                    }),
                    'options' => ['multiple' => true],
                    'pluginOptions' => [
                        'tags' => true,
                        'tokenSeparators' => [',', ' '],
                        'maximumInputLength' => 10
                    ],
                ]); ?>

                <?php echo $form->field($model, 'password')->passwordInput() ?>
                <?php echo $form->field($model, 'status')->dropDownList(User::statuses()) ?>
                <?php echo $form->field($model, 'roles')->checkboxList($roles) ?>
            </div>
            <div class="card-footer">
                <?php echo Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>
        </div>
    <?php ActiveForm::end() ?>
</div>
