<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Advanture */

$this->title = 'Создать урок';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advanture-create">

    <?= $this->render('_form', [
        'model' => $model,
        'module_id' => $module_id
    ]) ?>

</div>
