<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Advanture */

$this->title = 'Редактировать урок: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="advanture-update">

    <?= $this->render('_form', [
        'model' => $model,
        'module_id' => $module_id,
    ]) ?>

</div>
