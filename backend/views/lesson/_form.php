<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;

/* @var $this yii\web\View */
/* @var $model common\models\Advanture */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advanture-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'thumbnail')->widget(
        Upload::class,
        [
            'url' => ['/file/storage/upload'],
            'maxFileSize' => 5000000, // 5 MiB,
            'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
        ]
    ) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'video_link')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'short_content')->widget(
        \yii\imperavi\Widget::class,
        [
            'plugins' => ['fullscreen', 'fontcolor', 'video'],
            'options' => [
                'minHeight' => 400,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => true,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file/storage/upload-imperavi']),
            ],
        ]
    ) ?>
    <?php echo $form->field($model, 'description')->widget(
        \yii\imperavi\Widget::class,
        [
            'plugins' => ['fullscreen', 'fontcolor', 'video'],
            'options' => [
                'minHeight' => 400,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => true,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file/storage/upload-imperavi']),
            ],
        ]
    ) ?>

    <?php echo $form->field($model, 'file')->widget(
        Upload::class,
        [
            'url' => ['/file/storage/upload'],
            'maxFileSize' => 50000000000, // 5 MiB,
            'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png|doc|docx|zip|rar|pptx|pdf|svg)$/i'),
        ]
    ) ?>

    <?=$form->field($model, 'module')->hiddenInput(['value' => $module_id])->label(false); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
