<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Course */

$this->title = 'Редактировать курс: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="course-update">

    <?= $this->render('_form', [
        'model' => $model,
        'dataAdvantureProvider' => $dataAdvantureProvider,
        'dataWhoProvider' => $dataWhoProvider,
        'dataWhatLearhProvider' => $dataWhatLearhProvider,
        'dataModuleProvider' => $dataModuleProvider,
    ]) ?>

</div>
