<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "review".
 *
 * @property int $id
 * @property string $name
 * @property string|null $phone
 * @property string $comment
 * @property int|null $active
 * @property int|null $weight
 * @property string|null $email
 * @property int|null $date
 */
class Review extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','phone','email', 'comment'], 'required'],
            [['comment'], 'string'],
            [['active', 'weight'], 'integer'],
            [['name', 'phone', 'email', 'date'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'comment' => 'Отзыв',
            'active' => 'Опубликовано',
            'weight' => 'Сортировка',
            'email' => 'Email',
            'date' => 'Дата',
        ];
    }
}
