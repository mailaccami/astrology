<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use Yii;
use himiklab\sortablegrid\SortableGridBehavior;

/**
 * This is the model class for table "course".
 *
 * @property int $id
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $weight
 * @property string $title
 * @property string $slug
 * @property string|null $description
 * @property string|null $short_content
 * @property string|null $thumbnail_base_url
 * @property string|null $thumbnail_path
 * @property string|null $header
 * @property string|null $lasting
 * @property int $price
 * @property string $teacher_name
 * @property string|null $teacher_base_url
 * @property string|null $teacher_path
 * @property string|null $teacher_description
 * @property string|null $teacher_specialization
 * @property int|null $teacher_id
 * @property int|null $category_id
 *
 * @property Advanture[] $advantures
 * @property Module[] $modules
 * @property User $teacher
 * @property UserCourse[] $userCourses
 * @property WhatLearh[] $whatLearhs
 * @property Who[] $whos
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $thumbnail;
    public $teacher_img;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'title',
                'immutable' => true,
            ],
            [
                'class' => UploadBehavior::class,
                'attribute' => 'thumbnail',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            [
                'class' => UploadBehavior::class,
                'attribute' => 'teacher_img',
                'pathAttribute' => 'teacher_path',
                'baseUrlAttribute' => 'teacher_base_url',
            ],
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'weight'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'weight', 'teacher_id', 'category_id'], 'integer'],
            [['title'], 'required'],
            [['description','short_content', 'teacher_description'], 'string'],
            [['slug'], 'unique'],
            [['thumbnail', 'teacher_img'], 'safe'],
            [['title', 'thumbnail_base_url', 'thumbnail_path', 'header', 'lasting', 'teacher_base_url', 'teacher_path', 'teacher_specialization'], 'string', 'max' => 255],
            [['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['teacher_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'thumbnail' => 'Картинка',
            'teacher_img' => 'фото учителя',
            'slug' => 'URL',
            'weight' => 'Weight',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'short_content' => 'Краткое описание',
            'thumbnail_base_url' => 'Thumbnail Base Url',
            'thumbnail_path' => 'Thumbnail Path',
            'header' => 'Подзаголовок',
            'lasting' => 'Длительность',
            'price' => 'Цена',
            'teacher_name' => 'Имя учителя',
            'teacher_base_url' => 'Teacher Base Url',
            'teacher_path' => 'Teacher Path',
            'teacher_description' => 'Описание учителя',
            'teacher_specialization' => 'Специализация учителя',
            'teacher_id' => 'Учитель',
            'category_id' => 'Категория',
        ];
    }

    /**
     * Gets query for [[Advantures]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAdvantures()
    {
        return $this->hasMany(Advanture::className(), ['course_id' => 'id'])->orderBy(['weight'=>SORT_ASC]);
    }

    /**
     * Gets query for [[Modules]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModules()
    {
        return $this->hasMany(Module::className(), ['course_id' => 'id'])->orderBy(['weight'=>SORT_ASC]);
    }

    /**
     * Gets query for [[Teacher]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(User::className(), ['id' => 'teacher_id']);
    }

    /**
     * Gets query for [[UserCourses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserCourses()
    {
        return $this->hasMany(UserCourse::className(), ['course_id' => 'id']);
    }

    /**
     * Gets query for [[WhatLearhs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWhatLearhs()
    {
        return $this->hasMany(WhatLearh::className(), ['course_id' => 'id'])->orderBy(['weight'=>SORT_ASC]);
    }

    /**
     * Gets query for [[Whos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWhos()
    {
        return $this->hasMany(Who::className(), ['course_id' => 'id'])->orderBy(['weight'=>SORT_ASC]);
    }

    public function getImage($default = null)
    {
        return $this->thumbnail_path
            ? Yii::getAlias($this->thumbnail_base_url . '/' . $this->thumbnail_path)
            : $default;
    }

    public function getTeacherPhoto($default = null)
    {
        return $this->teacher_path
            ? Yii::getAlias($this->teacher_base_url . '/' . $this->teacher_path)
            : $default;
    }

}
