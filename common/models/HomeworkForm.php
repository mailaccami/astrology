<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "homework_form".
 *
 * @property int $id
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $user_id
 * @property int|null $teacher_id
 * @property string|null $message
 * @property string|null $lesson_name
 * @property string|null $file_base_url
 * @property string|null $file_path
 *
 * @property User $teacher
 * @property User $user
 */
class HomeworkForm extends \yii\db\ActiveRecord
{

    public $file;



    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'homework_form';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'file' => [
                'class' => UploadBehavior::class,
                'attribute' => 'file',
                'pathAttribute' => 'file_path',
                'baseUrlAttribute' => 'file_base_url'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'user_id', 'teacher_id'], 'integer'],
            [['message'], 'string'],
            [['message','user_id','teacher_id','lesson_name'], 'required'],
            [['file'], 'safe'],
            [['lesson_name', 'file_base_url', 'file_path'], 'string', 'max' => 255],
            [['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['teacher_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
            'teacher_id' => 'Teacher ID',
            'message' => 'Решение',
            'lesson_name' => 'Lesson Name',
            'file_base_url' => 'File Base Url',
            'file_path' => 'File Path',
        ];
    }

    /**
     * Gets query for [[Teacher]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(User::className(), ['id' => 'teacher_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getFile1($default = null)
    {
        return $this->file_path
            ? Yii::getAlias($this->file_base_url . '/' . $this->file_path)
            : $default;
    }

}
