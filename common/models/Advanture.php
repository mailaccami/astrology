<?php

namespace common\models;

use himiklab\sortablegrid\SortableGridBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "advanture".
 *
 * @property int $id
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $weight
 * @property string $title
 * @property string|null $description
 * @property string|null $thumbnail_base_url
 * @property string|null $thumbnail_path
 * @property int|null $course_id
 *
 * @property Course $course
 */
class Advanture extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advanture';
    }

    public $thumbnail;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => UploadBehavior::class,
                'attribute' => 'thumbnail',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'weight'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'weight', 'course_id'], 'integer'],
            [['title'], 'required'],
            [['description'], 'string'],
            [['thumbnail'], 'safe'],
            [['title', 'thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 255],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'weight' => 'Weight',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'thumbnail' => 'Картинка',
            'thumbnail_base_url' => 'Thumbnail Base Url',
            'thumbnail_path' => 'Thumbnail Path',
            'course_id' => 'Курс',
        ];
    }

    /**
     * Gets query for [[Course]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    public function getImage($default = null)
    {
        return $this->thumbnail_path
            ? Yii::getAlias($this->thumbnail_base_url . '/' . $this->thumbnail_path)
            : $default;
    }

}
