<?php

namespace common\models;

use himiklab\sortablegrid\SortableGridBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "lesson".
 *
 * @property int $id
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $weight
 * @property string $title
 * @property string $short_content
 * @property string|null $thumbnail_base_url
 * @property string|null $thumbnail_path
 * @property string|null $description
 * @property string $video_link
 * @property string|null $file_base_url
 * @property string|null $file_path
 * @property int|null $module
 *
 * @property Module $module0
 */
class Lesson extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson';
    }

    public $file;
    public $thumbnail;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => UploadBehavior::class,
                'attribute' => 'file',
                'pathAttribute' => 'file_path',
                'baseUrlAttribute' => 'file_base_url',
            ],
            [
                'class' => UploadBehavior::class,
                'attribute' => 'thumbnail',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'weight'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'weight', 'module'], 'integer'],
            [['title', 'video_link'], 'required'],
            [['description', 'short_content'], 'string'],
            [['file', 'thumbnail'], 'safe'],
            [['title', 'video_link', 'file_base_url', 'file_path','thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 255],
            [['module'], 'exist', 'skipOnError' => true, 'targetClass' => Module::className(), 'targetAttribute' => ['module' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'weight' => 'Weight',
            'thumbnail' => 'Картинка',
            'title' => 'Заголовок',
            'file' => 'Файл',
            'description' => 'Описание',
            'short_content' => 'Краткое описание',
            'video_link' => 'Ссылка на видео',
            'file_base_url' => 'File Base Url',
            'file_path' => 'File Path',
            'module' => 'Модуль',
        ];
    }

    /**
     * Gets query for [[Module0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModule0()
    {
        return $this->hasOne(Module::className(), ['id' => 'module']);
    }

    public function getNum(){
        $lessonNum = 0;
        foreach ($this->module0->lessons as $lesson) {
            $lessonNum++;
            if($lesson->id == $this->id) break;
        }
        return $lessonNum;
    }

    public function getFileUrl($default = null)
    {
        return $this->file_path
            ? Yii::getAlias($this->file_base_url . '/' . $this->file_path)
            : $default;
    }

    public function getImage($default = null)
    {
        return $this->thumbnail_path
            ? Yii::getAlias($this->thumbnail_base_url . '/' . $this->thumbnail_path)
            : $default;
    }

}
