<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%who}}`.
 */
class m201115_171448_create_who_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%who}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->defaultValue(null),
            'updated_at' => $this->integer()->defaultValue(null),
            'weight' => $this->integer()->defaultValue(0),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%who}}');
    }
}
