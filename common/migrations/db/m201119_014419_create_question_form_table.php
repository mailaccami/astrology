<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%question_form}}`.
 */
class m201119_014419_create_question_form_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%question_form}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->defaultValue(null),
            'updated_at' => $this->integer()->defaultValue(null),
            'name' => $this->string()->defaultValue(null),
            'phone' => $this->string()->defaultValue(null),
            'message' => $this->text()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%question_form}}');
    }
}
