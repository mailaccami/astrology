<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%course}}`.
 */
class m201115_190333_drop_teacher_description_column_from_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%course}}', 'teacher_description');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%course}}', 'teacher_description', $this->string());
    }
}
