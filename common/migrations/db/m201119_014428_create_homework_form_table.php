<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%homework_form}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m201119_014428_create_homework_form_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%homework_form}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->defaultValue(null),
            'updated_at' => $this->integer()->defaultValue(null),
            'user_id' => $this->integer(),
            'teacher_id' => $this->integer(),
            'message' => $this->text()->defaultValue(null),
            'lesson_name' => $this->string()->defaultValue(null),
            'file_base_url' => $this->string()->defaultValue(null),
            'file_path' => $this->string()->defaultValue(null),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-homework_form-user_id}}',
            '{{%homework_form}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-homework_form-user_id}}',
            '{{%homework_form}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `teacher_id`
        $this->createIndex(
            '{{%idx-homework_form-teacher_id}}',
            '{{%homework_form}}',
            'teacher_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-homework_form-teacher_id}}',
            '{{%homework_form}}',
            'teacher_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-homework_form-user_id}}',
            '{{%homework_form}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-homework_form-user_id}}',
            '{{%homework_form}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-homework_form-teacher_id}}',
            '{{%homework_form}}'
        );

        // drops index for column `teacher_id`
        $this->dropIndex(
            '{{%idx-homework_form-teacher_id}}',
            '{{%homework_form}}'
        );

        $this->dropTable('{{%homework_form}}');
    }
}
