<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%who}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%course}}`
 */
class m201115_173511_add_course_id_column_to_who_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%who}}', 'course_id', $this->integer());

        // creates index for column `course_id`
        $this->createIndex(
            '{{%idx-who-course_id}}',
            '{{%who}}',
            'course_id'
        );

        // add foreign key for table `{{%course}}`
        $this->addForeignKey(
            '{{%fk-who-course_id}}',
            '{{%who}}',
            'course_id',
            '{{%course}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%course}}`
        $this->dropForeignKey(
            '{{%fk-who-course_id}}',
            '{{%who}}'
        );

        // drops index for column `course_id`
        $this->dropIndex(
            '{{%idx-who-course_id}}',
            '{{%who}}'
        );

        $this->dropColumn('{{%who}}', 'course_id');
    }
}
