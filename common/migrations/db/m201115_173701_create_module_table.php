<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%module}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%course}}`
 */
class m201115_173701_create_module_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%module}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->defaultValue(null),
            'updated_at' => $this->integer()->defaultValue(null),
            'course_id' => $this->integer(),
            'weight' => $this->integer()->defaultValue(0),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->defaultValue(null),
        ]);

        // creates index for column `course_id`
        $this->createIndex(
            '{{%idx-module-course_id}}',
            '{{%module}}',
            'course_id'
        );

        // add foreign key for table `{{%course}}`
        $this->addForeignKey(
            '{{%fk-module-course_id}}',
            '{{%module}}',
            'course_id',
            '{{%course}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%course}}`
        $this->dropForeignKey(
            '{{%fk-module-course_id}}',
            '{{%module}}'
        );

        // drops index for column `course_id`
        $this->dropIndex(
            '{{%idx-module-course_id}}',
            '{{%module}}'
        );

        $this->dropTable('{{%module}}');
    }
}
