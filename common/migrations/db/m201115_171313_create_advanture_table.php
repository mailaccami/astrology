<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%advanture}}`.
 */
class m201115_171313_create_advanture_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%advanture}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->defaultValue(null),
            'updated_at' => $this->integer()->defaultValue(null),
            'weight' => $this->integer()->defaultValue(0),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->defaultValue(null),
            'thumbnail_base_url' => $this->string()->defaultValue(null),
            'thumbnail_path' => $this->string()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%advanture}}');
    }
}
