<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%course}}`.
 */
class m210127_011755_drop_teacher_name_column_from_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%course}}', 'teacher_name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%course}}', 'teacher_name', $this->string());
    }
}
