<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lesson}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%module}}`
 */
class m201115_174142_create_lesson_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lesson}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->defaultValue(null),
            'updated_at' => $this->integer()->defaultValue(null),
            'weight' => $this->integer()->defaultValue(0),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->defaultValue(null),
            'video_link' => $this->string()->notNull(),
            'file_base_url' => $this->string()->defaultValue(null),
            'file_path' => $this->string()->defaultValue(null),
            'module' => $this->integer(),
        ]);

        // creates index for column `module`
        $this->createIndex(
            '{{%idx-lesson-module}}',
            '{{%lesson}}',
            'module'
        );

        // add foreign key for table `{{%module}}`
        $this->addForeignKey(
            '{{%fk-lesson-module}}',
            '{{%lesson}}',
            'module',
            '{{%module}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%module}}`
        $this->dropForeignKey(
            '{{%fk-lesson-module}}',
            '{{%lesson}}'
        );

        // drops index for column `module`
        $this->dropIndex(
            '{{%idx-lesson-module}}',
            '{{%lesson}}'
        );

        $this->dropTable('{{%lesson}}');
    }
}
