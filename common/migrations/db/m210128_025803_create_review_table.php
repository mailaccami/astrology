<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%review}}`.
 */
class m210128_025803_create_review_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%review}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'phone' => $this->string(),
            'comment' => $this->text()->notNull(),
            'active' => $this->integer()->defaultValue(0),
            'weight' => $this->integer()->defaultValue(0),
            'email' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%review}}');
    }
}
