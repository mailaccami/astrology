<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%course}}`.
 */
class m210127_011627_drop_price_column_from_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%course}}', 'price');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%course}}', 'price', $this->integer());
    }
}
