<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%course}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m201115_173145_create_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%course}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->defaultValue(null),
            'updated_at' => $this->integer()->defaultValue(null),
            'weight' => $this->integer()->defaultValue(0),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->defaultValue(null),
            'thumbnail_base_url' => $this->string()->defaultValue(null),
            'thumbnail_path' => $this->string()->defaultValue(null),
            'header' => $this->string()->defaultValue(null),
            'lasting' => $this->string()->defaultValue(null),
            'price' => $this->integer()->notNull(),
            'teacher_name' => $this->string()->notNull(),
            'teacher_base_url' => $this->string()->defaultValue(null),
            'teacher_path' => $this->string()->defaultValue(null),
            'teacher_description' => $this->string()->defaultValue(null),
            'teacher_specialization' => $this->string()->defaultValue(null),
            'teacher_id' => $this->integer(),
        ]);

        // creates index for column `teacher_id`
        $this->createIndex(
            '{{%idx-course-teacher_id}}',
            '{{%course}}',
            'teacher_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-course-teacher_id}}',
            '{{%course}}',
            'teacher_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-course-teacher_id}}',
            '{{%course}}'
        );

        // drops index for column `teacher_id`
        $this->dropIndex(
            '{{%idx-course-teacher_id}}',
            '{{%course}}'
        );

        $this->dropTable('{{%course}}');
    }
}
