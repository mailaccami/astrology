<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%what_learh}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%course}}`
 */
class m201115_173520_add_course_id_column_to_what_learh_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%what_learh}}', 'course_id', $this->integer());

        // creates index for column `course_id`
        $this->createIndex(
            '{{%idx-what_learh-course_id}}',
            '{{%what_learh}}',
            'course_id'
        );

        // add foreign key for table `{{%course}}`
        $this->addForeignKey(
            '{{%fk-what_learh-course_id}}',
            '{{%what_learh}}',
            'course_id',
            '{{%course}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%course}}`
        $this->dropForeignKey(
            '{{%fk-what_learh-course_id}}',
            '{{%what_learh}}'
        );

        // drops index for column `course_id`
        $this->dropIndex(
            '{{%idx-what_learh-course_id}}',
            '{{%what_learh}}'
        );

        $this->dropColumn('{{%what_learh}}', 'course_id');
    }
}
