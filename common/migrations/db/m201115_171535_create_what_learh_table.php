<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%what_learh}}`.
 */
class m201115_171535_create_what_learh_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%what_learh}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->defaultValue(null),
            'updated_at' => $this->integer()->defaultValue(null),
            'weight' => $this->integer()->defaultValue(0),
            'title' => $this->string()->notNull(),
            'thumbnail_base_url' => $this->string()->defaultValue(null),
            'thumbnail_path' => $this->string()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%what_learh}}');
    }
}
