<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%page}}`.
 */
class m210127_021941_add_short_content_column_to_page_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%page}}', 'short_content', $this->text());
        $this->addColumn('{{%page}}', 'thumbnail_base_url', $this->string()->defaultValue(null));
        $this->addColumn('{{%page}}', 'thumbnail_path', $this->string()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%page}}', 'short_content');
    }
}
