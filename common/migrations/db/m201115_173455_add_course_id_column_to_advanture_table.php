<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%advanture}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%course}}`
 */
class m201115_173455_add_course_id_column_to_advanture_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%advanture}}', 'course_id', $this->integer());

        // creates index for column `course_id`
        $this->createIndex(
            '{{%idx-advanture-course_id}}',
            '{{%advanture}}',
            'course_id'
        );

        // add foreign key for table `{{%course}}`
        $this->addForeignKey(
            '{{%fk-advanture-course_id}}',
            '{{%advanture}}',
            'course_id',
            '{{%course}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%course}}`
        $this->dropForeignKey(
            '{{%fk-advanture-course_id}}',
            '{{%advanture}}'
        );

        // drops index for column `course_id`
        $this->dropIndex(
            '{{%idx-advanture-course_id}}',
            '{{%advanture}}'
        );

        $this->dropColumn('{{%advanture}}', 'course_id');
    }
}
