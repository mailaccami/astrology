<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%course}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%category}}`
 */
class m210127_005319_add_category_id_column_to_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%course}}', 'category_id', $this->integer());

        // creates index for column `category_id`
        $this->createIndex(
            '{{%idx-course-category_id}}',
            '{{%course}}',
            'category_id'
        );

        // add foreign key for table `{{%category}}`
        $this->addForeignKey(
            '{{%fk-course-category_id}}',
            '{{%course}}',
            'category_id',
            '{{%category}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%category}}`
        $this->dropForeignKey(
            '{{%fk-course-category_id}}',
            '{{%course}}'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            '{{%idx-course-category_id}}',
            '{{%course}}'
        );

        $this->dropColumn('{{%course}}', 'category_id');
    }
}
