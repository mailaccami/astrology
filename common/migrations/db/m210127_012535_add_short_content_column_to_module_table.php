<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%module}}`.
 */
class m210127_012535_add_short_content_column_to_module_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%module}}', 'short_content', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%module}}', 'short_content');
    }
}
